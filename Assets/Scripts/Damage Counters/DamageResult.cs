﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DamageResult : MonoBehaviour
{
	public Text high;
	public Text low;

	private DamageCounter damageCounter;	

	public void Initialize(DamageCounter DC, Ship target )
	{
		damageCounter = DC;
		high.text = DC.amount.ToString();
		
		if(DC.isCrew) 	low.text="C";
		if(DC.isMast) 	low.text="M";
		if(DC.isFire) 	low.text="F";
		if(DC.isLeak) 	low.text="L";
		if(DC.isRudder) low.text="R";
		if(DC.isSails) 	low.text="S";

		target.RecieveDamage(this);
	}

	public int GetAmount()
	{
		return damageCounter.amount;
	}

	public void ShowDamage()
	{
		MainSceneUIManager UIMan = MainSceneUIManager.Instance;
		GetComponent<RectTransform>().SetParent(UIMan.damageResultsContainer);
	}
}
