﻿using System.Linq;
using UnityEngine;
using System.Collections;

public class DamageCounterGroup : ScriptableObject 
{
	public DamageCounter.Type type;
	public DamageCounter[] damageCounters;
	private DamageResult[] damageResults;
	private bool isCurrent;

	public DamageResult DoDamage(Ship target)
	{
		int randomIndex = Random.Range(0,damageCounters.Length-1);
		return damageCounters[randomIndex].DoDamage(target);
	}

	public void DoDamage(Ship target, int number)
	{
		damageResults = new DamageResult[number];
		for (int i=0; i<number; i++) damageResults[i] = DoDamage(target);

		ShowDamageResults();
	}

	public bool IsCurrent()
	{
		return isCurrent;
	}

	public void SetCurrent(bool b)
	{
		isCurrent=b;
	}

	public void ShowDamageResults()
	{
		damageResults = damageResults.OrderBy(dr => dr.GetAmount()).ToArray();
		for (int i=0; i<damageResults.Length; i++) damageResults[i].ShowDamage();
	}

}
