﻿using UnityEngine;
using System.Collections;

public class DamageCounter : ScriptableObject 
{
	public enum Type { red, purple, yellow, orange, blue}
	public Type type;
	public bool isCrew, isMast, isFire, isLeak, isRudder, isSails;
	public int amount;

	public DamageResult DoDamage(Ship target)
	{
		MainSceneUIManager UIMan = MainSceneUIManager.Instance;
		DamageResult DR = (DamageResult) Instantiate(UIMan.damageResultPrefab);
		DR.Initialize(this, target);
		return DR;
	}
}
