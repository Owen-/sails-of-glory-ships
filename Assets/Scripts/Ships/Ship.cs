﻿using UnityEngine;
using UnityEngine.UI; ///
///
//
//
// REMOVE THISSSSS^^^^^
//
//
//
//
///
using System.Collections;
using System.Collections.Generic;

public class Ship 
{
	private ShipType shipType;
	//public Text fireCounter, mastCounter, leakCounter, rudderCounter, sailsCounter, damageCounter;
	public int currentDamage, currentFires, currentLeaks, currentBrokenRudders, currentBrokenMasts, currentSails, currentHull, currentCrew, destroyedBoxes;
	public int LG, RG, a1, a2, a3, a4, s1, s2, s3, s4;

	public string shipName 				{ get { return shipType.shipName; } }
	public string deck 					{ get { return shipType.deck; } } 		// please convert me to ints -.-
	public string burden 				{ get { return shipType.burden; } } 	// please convert me to ints -.-
	public string veer 					{ get { return shipType.veer; } } 		// please convert me to ints -.-
	public string shortName 			{ get {	return shipType.shortName != "" ? shipType.shortName : shipType.shipName; } }
	public List<DamageBox> damageBoxes 	{ get { return shipType.damageBoxes; } } // damage boxes are actually the ships data! not the damaged boxes
	
	public Ship(ShipType ST)
	{
		shipType = ST;
	}

	public void RecieveDamage(DamageResult damage)
	{
		if ((d.amount+currentDamage)>=int.Parse(burden)) destroyBox();
		else 
		{
			currentDamage+=d.amount;
			damageCounter.text=currentDamage.ToString();
		}

		if(d.isCrew) destroyCrew();
		if(d.isMast) mast(1);
		if(d.isLeak) leak(1);
		if(d.isRudder) rudder(1);
		if(d.isFire) fire(1);
		if(d.isSails) sails(1);

		if (currentDamage != 0 ) damageBox();
	}

		public List<GameObject> dbs;

	public void destroyBox()
	{
		currentDamage=0;
		currentHull++;
		for (int i=0;i<dbs.Count;i++) 
		{
			if(!dbs[i].GetComponent<DamageBoxGO>().topMask.enabled || dbs[i].GetComponent<DamageBoxGO>().topMask.color==new Color(1f, 0f, 0f, .75f))
			{
				dbs[i].GetComponent<DamageBoxGO>().topMask.enabled=true; 
				dbs[i].GetComponent<DamageBoxGO>().topMask.color=new Color(0f,0f,0f,.75f);
				return;
			}
		}
	}

	public void damageBox()
	{
		for (int i=0;i<dbs.Count;i++) 
		{
			if(dbs[i].GetComponent<DamageBoxGO>().topMask.color == new Color(1f, 0f, 0f, .75f)) return;

			if(!dbs[i].GetComponent<DamageBoxGO>().topMask.enabled)
			{
				dbs[i].GetComponent<DamageBoxGO>().topMask.enabled=true; 
				dbs[i].GetComponent<DamageBoxGO>().topMask.color=new Color(1f, 0f, 0f, .75f);
				return;
			}
		}
	}

	public void destroyCrew()
	{
		crew(true);
		for (int i=0; i<dbs.Count;i++) if(!dbs[i].GetComponent<DamageBoxGO>().bottomMask.enabled) {dbs[i].GetComponent<DamageBoxGO>().bottomMask.enabled=true; return;}
	}

	public void crew(bool b)
	{
		if (b) currentCrew++;
		else currentCrew--;
	}

	public void hull(bool b)
	{
		if (b) currentHull++;
		else currentHull--;
	}

	public void mast(int i)
	{
		currentBrokenMasts=int.Parse(mastCounter.text)+i;
		mastCounter.text=currentBrokenMasts.ToString();
	}

	public void leak(int i)
	{
		currentLeaks=int.Parse(leakCounter.text)+i;
		leakCounter.text=currentLeaks.ToString();
	}

	public void rudder(int i)
	{
		currentBrokenRudders=int.Parse(rudderCounter.text)+i;
		rudderCounter.text=currentBrokenRudders.ToString();
	}

	public void fire(int i)
	{
		currentFires=int.Parse(fireCounter.text)+i;
		fireCounter.text=currentFires.ToString();
	}

	public void sails(int i)
	{
		currentSails=int.Parse(sailsCounter.text)+i;
		sailsCounter.text=currentSails.ToString();
	}

	public void damage(int i)
	{
		currentDamage=int.Parse(damageCounter.text)+i;
		damageCounter.text=currentDamage.ToString();
	}
}