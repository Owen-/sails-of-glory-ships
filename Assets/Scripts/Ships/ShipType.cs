﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ShipType : ScriptableObject 
{
	public string shipName;
	public string deck;
	public string shortName;
	public string burden;
	public string veer;
	
	public List<DamageBox> damageBoxes;

	private Toggle isActiveToggle;

	public void SetToggle(Toggle T)
	{
		isActiveToggle = T;
	}
	public bool IsActive()
	{
		return isActiveToggle.isOn;
	}
}
