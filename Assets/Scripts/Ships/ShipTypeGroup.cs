﻿using UnityEngine;
using System.Collections;

public class ShipTypeGroup : ScriptableObject 
{
	public ShipType[] shipTypes;
}