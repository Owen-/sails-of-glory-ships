﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ToggleImages : MonoBehaviour 
{
	public List<Sprite> textures;
	public int index;
	public Ship ship; // this needs to just be specified in main ui manager

	void Start()
	{
		index=0;
	}
	
	public void Change()
	{
		if (index+1>=textures.Count) index=0;
		else index++;
		MainSceneUIManager.Instance.UpdateShip(this.gameObject.GetComponent<Image>(), ship);
	}

	public void SetIndex(int i)
	{
		index=i;
		MainSceneUIManager.Instance.UpdateShip(this.gameObject.GetComponent<Image>(), ship);
	
	}
	
	void Update () 
	{
		if (textures.Count >0) if (this.gameObject.GetComponent<Image>().sprite!=textures[index]) this.gameObject.GetComponent<Image>().sprite=textures[index];
	}
}
