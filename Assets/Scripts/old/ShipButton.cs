﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShipButton : MonoBehaviour 
{
	public Text t;
	private Ship ship;

	public void SetShip(Ship S)
	{
		ship = S;
		t.text = S.shortName;
		MainSceneUIManager.Instance.SetShip(S);
	}
}
