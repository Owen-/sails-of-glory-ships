﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FireGun : MonoBehaviour 
{
	public Image I;
	public Image reload;
	public Color activeCol, passiveCol;
	public Button B;
	public bool active;

	void start()
	{
		active=true;
	}


	public void fire()
	{
		if(active)
		{
			if(I.color!=passiveCol)
			{
				I.color=passiveCol;
			}
			else 
			{
				B.enabled=false;
				reload.enabled=true;
				active=!active;

			}
		}
		else
		{
			I.color=activeCol;
			reload.enabled=false;
			B.enabled=true;
			active=!active;
		}
	}
}
