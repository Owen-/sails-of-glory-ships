﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class DamageBox
{
	public string frontBroadside;
	public string fullBroadside;
	public string rearBroadside;
	public string crewActions;
	public string numberOfCrew;
}