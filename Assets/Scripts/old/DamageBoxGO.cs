﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DamageBoxGO : MonoBehaviour 
{
	public Text FrontBroadside;
	public Text FullBroadside;
	public Text RearBroadside;
	public Text CrewActions;
	public Text NumberOfCrew;
	public Image topMask;
	public Image bottomMask;

	public void SetDB(DamageBox db)
	{
		FrontBroadside.text=db.frontBroadside;
		FullBroadside.text=db.fullBroadside;
		RearBroadside.text=db.rearBroadside;
		CrewActions.text=db.crewActions;
		NumberOfCrew.text=db.numberOfCrew;
	}
}
