﻿using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class MenuSceneUIManager : MonoBehaviour 
{
	public ShipTypeGroup shipTypeGroup;
	public RectTransform shipListParent;
	public GameObject shipButtonPrefab;

	[MenuItem("Assets/Create/Ships/New Ship Type")]
	public static void NewShipType ()
	{
		ScriptableObjectUtility.CreateAsset<ShipType> ();
	}

	[MenuItem("Assets/Create/Ships/New Ship Type Group")]
	public static void NewShipTypeGroup ()
	{
		ScriptableObjectUtility.CreateAsset<ShipTypeGroup> ();
	}

	private void Start () 
	{
		InstantiateButtons();
	}

	public void UseSelectedShips()
	{
		FleetManager.Instance.ships.Clear();
		foreach (ShipType ST in shipTypeGroup.shipTypes)
			if (ST.IsActive()) FleetManager.Instance.ships.Add(new Ship(ST));
		
		SceneManager.LoadScene("main");
	}

	public void Exit()
	{
		Application.Quit();
	}

	private void InstantiateButtons()
	{
		foreach(ShipType ST in shipTypeGroup.shipTypes) 
		{
			GameObject go = Instantiate(shipButtonPrefab);
			go.GetComponent<RectTransform>().GetChild(0).GetComponent<Text>().text=ST.name;
			go.GetComponent<RectTransform>().SetParent(shipListParent);

			ST.SetToggle(go.GetComponent<Toggle>());
		}
	}
}
