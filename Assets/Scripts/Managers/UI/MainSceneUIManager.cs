﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class MainSceneUIManager : MonoBehaviour
{
	public static MainSceneUIManager Instance;

	public List<GameObject> dbs;
	public Ship currentShip;
	public List<ToggleImages> tis;
	public RectTransform shipContainer;
	public GameObject shipButtonPrefab;

	public DamageCounterGroup[] damageCounterGroups;
	public Text shipFullName, Deck, Burden, Veer, f, r, m, l, sterriblename, dam;
	public Text fireCounter, mastCounter, leakCounter, rudderCounter, sailsCounter, damageCounter;
	public Image LeftGun, RightGun, A1, A2, A3, A4, S1, S2, S3, S4;
	public RectTransform damageBoxContainer;
	public RectTransform damageResultsContainer;
	public GameObject damageBoxPrefab;
	public Text damageCounterNumber;
	public DamageResult damageResultPrefab;
	

	private List<Ship> ships;

	private void Start()
	{
		if (Instance == null) Instance = this; // maybe I should just expand the singleton script to not inc ddol

		FleetManager.Instance.MakeButtons(shipContainer);
		ships=FleetManager.Instance.ships;
		SetShip(ships[0]);
	}

	public void DoDamage()
	{
		int number = int.Parse(damageCounterNumber.text);

		if(number > 0)
			foreach(DamageCounterGroup DCG in damageCounterGroups)
				if (DCG.IsCurrent()) 
					DCG.DoDamage(currentShip, number);

	}

	public void CycleDamageType()
	{
		int current = 0;
		for (int i = 0; i<damageCounterGroups.Length; i++)
		{
			if (damageCounterGroups[i].IsCurrent())
			{				
				if (i==damageCounterGroups.Length-1) current=0;
				else current = i+1;
			}
		}

		for (int i = 0; i<damageCounterGroups.Length; i++)
			damageCounterGroups[i].SetCurrent(i==current);
	}

	public void UpdateShip(Image I, Ship s)
	{
		if (I==LeftGun) 	s.LG=I.gameObject.GetComponent<ToggleImages>().index;
		if (I==RightGun) 	s.RG=I.gameObject.GetComponent<ToggleImages>().index;
		if (I==A1) 			s.a1=I.gameObject.GetComponent<ToggleImages>().index;
		if (I==A2) 			s.a2=I.gameObject.GetComponent<ToggleImages>().index;
		if (I==A3) 			s.a3=I.gameObject.GetComponent<ToggleImages>().index;
		if (I==A4) 			s.a4=I.gameObject.GetComponent<ToggleImages>().index;
		if (I==S1) 			s.s1=I.gameObject.GetComponent<ToggleImages>().index;
		if (I==S2) 			s.s2=I.gameObject.GetComponent<ToggleImages>().index;
		if (I==S3) 			s.s3=I.gameObject.GetComponent<ToggleImages>().index;
		if (I==S4) 			s.s4=I.gameObject.GetComponent<ToggleImages>().index;

	}

	public void SetShip(string s)
	{
		for (int i=0; i<ships.Count; i++)
		{
			if (ships[i].shortName==s) SetShip(s);
		}
	}

	public void SetShip(Ship s)
	{
		currentShip=s;
		//foreach(ToggleImages ti in tis) ti.ship=s;
		LeftGun.gameObject.GetComponent<ToggleImages>().index=s.LG;
		RightGun.gameObject.GetComponent<ToggleImages>().index=s.RG;
		A1.gameObject.GetComponent<ToggleImages>().index=s.a1;
		A2.gameObject.GetComponent<ToggleImages>().index=s.a2;
		A3.gameObject.GetComponent<ToggleImages>().index=s.a3;
		A4.gameObject.GetComponent<ToggleImages>().index=s.a4;
		S1.gameObject.GetComponent<ToggleImages>().index=s.s1;
		S2.gameObject.GetComponent<ToggleImages>().index=s.s2;
		S3.gameObject.GetComponent<ToggleImages>().index=s.s3;
		S4.gameObject.GetComponent<ToggleImages>().index=s.s4;
		foreach (GameObject g in dbs) Destroy(g);
		dbs.Clear();
		shipFullName.text = s.shipName;
		Deck.text = "D:"+s.deck;
		Burden.text = "B:"+s.burden;
		Veer.text = "V:"+s.veer;
		f.text = s.currentFires.ToString();
		r.text = s.currentBrokenRudders.ToString();
		l.text = s.currentLeaks.ToString();
		m.text = s.currentBrokenMasts.ToString();
		sterriblename.text = s.currentSails.ToString();
		dam.text = s.currentDamage.ToString();

		foreach (DamageBox db in s.damageBoxes)
		{
			GameObject go = Instantiate(damageBoxPrefab);
			go.GetComponent<DamageBoxGO>().SetDB(db);
			go.GetComponent<RectTransform>().SetParent(damageBoxContainer);
			dbs.Add(go);
		}

		int tempHull = 0; 
		int tempCrew = 0;
		int tempDam = s.currentDamage;

		for(int j =0; j<s.currentHull;j++) tempHull++;
		for(int k =0; k<s.currentCrew;k++) tempCrew++;

		s.currentHull=0;
		s.currentCrew=0;

		for(int j =0; j<tempHull; j++) currentShip.destroyBox();
		for(int j =0; j<tempCrew; j++) currentShip.destroyCrew();

		s.currentDamage=tempDam;

		if( s.currentDamage > 0) currentShip.damageBox();

	}

	public void QuitToMenu()
	{
		SceneManager.LoadScene("mainmenu");
	}
	
}
