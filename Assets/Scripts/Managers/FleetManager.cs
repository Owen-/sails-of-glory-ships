using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FleetManager : Singleton<FleetManager> 
{
	public List<Ship> ships;
	public GameObject shipButtonPrefab;

	public override void Start()
	{
		ships=new List<Ship>();
		base.Start();
	}

	public void MakeButtons(Transform container)
	{
		for (int i=0; i<ships.Count; i++)
		{
			GameObject go = Instantiate(shipButtonPrefab);
			go.GetComponent<ShipButton>().SetShip(ships[i]);
			go.GetComponent<RectTransform>().SetParent(container);
		}
	}
}
 